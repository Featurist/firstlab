﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Serialization
{
    class StartJson
    {
        public int jsonRun()
        {
            //{"K":10,"Sums":[1.01,2.02],"Muls":[1,4]}
            String k;

            k = Console.ReadLine();
            Input input = JsonConvert.DeserializeObject<Input>(k);

            /*Console.WriteLine(input.K);
            for (int i = 0; i < input.Sums.Length; i++)
            {
                Console.WriteLine(input.Sums[i]);
            }
            for (int i = 0; i < input.Muls.Length; i++)
            {
                Console.WriteLine(input.Muls[i]);
            }*/

            Output output = new Output();

            for (int i = 0; i < input.Sums.Length; i++)
            {
                output.SumResult += input.Sums[i];
            }
            output.SumResult = output.SumResult * input.K;
            //Console.WriteLine(output.SumResult);

            int j = 1;
            
            foreach(var item in input.Muls)
            {
                j *= item;
            }
            output.MulResult = j;
            //Console.WriteLine(output.MulResult);

            List<decimal> sort = new List<decimal> { };
            for (int i = 0; i < input.Sums.Length; i++)
            {
                sort.Add(sort.Count + 1);
                sort[i] = input.Sums[i];
            }
            for (int i = input.Sums.Length; i < (input.Muls.Length + input.Sums.Length); i++)
            {
                sort.Add(sort.Count + 1);
                sort[i] = input.Muls[i - input.Sums.Length];
            }
            sort.Sort();
            decimal[] newSort = new decimal[sort.Count];
            for (int i = 0; i < sort.Count; i++)
            {
                newSort[i] = sort[i];
                output.SortedInputs = newSort;
            }

            string serialize = JsonConvert.SerializeObject(output);
            Console.WriteLine(serialize);
            //Console.Read();
            return 0;
        }
    }

}
