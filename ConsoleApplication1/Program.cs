﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
//using ConsoleApplication1;

namespace Serialization
{
    class Program
    {
        static void Main(string[] args)
        {
            StartJson startJson = new StartJson();
            StartXml startXml = new StartXml();
            if (Console.ReadLine() == "Json")
            {
                int result = startJson.jsonRun();
            }
            else
            {
                int result = startXml.xmlRun();
            }

        }

    }
}
