﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Serialization
{
    class StartXml
    {
        //todo: очень много дублирования, не должно быть ни одной повотряющся строчки типа  output.SumResult = output.SumResult * input.K;
        //todo: нужно провести нвоую декомпозицию классов, логику связанную с арфимтеическими операциями выделить в отлдельный метод, логику с сериалищзацией вынести в две реализации одногго и того же интерфейса ISerializer

        interface ISerializer
        {
            string Serialize<T>(T obj);
            T Deserialize<T>(string data);
        }


        public int xmlRun()
        {
            String xml;
            xml = Console.ReadLine();//"<Input><K>10</K><Sums><decimal>1.01</decimal><decimal>2.02</decimal></Sums><Muls><int>1</int><int>4</int></Muls></Input>";
            var serializer = new XmlSerializer(typeof(Input));
            Input input = new Input();

            using (TextReader reader = new StringReader(xml))
            {
                input = (Input)serializer.Deserialize(reader);
            }

            //Input input = new Input();
            /*Console.WriteLine(result.K);
            for (int i = 0; i < result.Sums.Length; i++)
            {
                Console.WriteLine(result.Sums[i]);
            }
            for (int i = 0; i < result.Muls.Length; i++)
            {
                Console.WriteLine(result.Muls[i]);
            }*/

            Output output = new Output();

            for (int i = 0; i < input.Sums.Length; i++)
            {
                output.SumResult += input.Sums[i];
            }
            output.SumResult = output.SumResult * input.K;
            //Console.WriteLine(output.SumResult);

            int j = 1;

            foreach (var item in input.Muls)
            {
                j *= item;
            }
            output.MulResult = j;
            //Console.WriteLine(output.MulResult);

            List<decimal> sort = new List<decimal> { };
            for (int i = 0; i < input.Sums.Length; i++)
            {
                sort.Add(sort.Count + 1);
                sort[i] = input.Sums[i];
            }
            for (int i = input.Sums.Length; i < (input.Muls.Length + input.Sums.Length); i++)
            {
                sort.Add(sort.Count + 1);
                sort[i] = input.Muls[i - input.Sums.Length];
            }
            sort.Sort();
            decimal[] newSort = new decimal[sort.Count];
            for (int i = 0; i < sort.Count; i++)
            {
                newSort[i] = sort[i];
                output.SortedInputs = newSort;
            }


            var ser = new XmlSerializer(typeof(Output));

            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            var sw = new StringWriter();
            var xmlWriter = XmlWriter.Create(sw, new XmlWriterSettings() { OmitXmlDeclaration = true });

            ser.Serialize(xmlWriter, output, ns);
            string xml1 = sw.ToString();
            Console.WriteLine(xml1);
            /*
            XmlSerializer newSerializer = new XmlSerializer(typeof(Output));
            StringWriter writer = new StringWriter();
            var xns = new XmlSerializerNamespaces();
            xns.Add("", "");
            newSerializer.Serialize(writer, output, xns);
            string serializedXML = writer.ToString();
            Console.WriteLine(serializedXML);
            //Console.Read();
            */

            return 0;
        }
        /*
        public string SerializeToString(T value)
        {
            var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var serializer = new XmlSerializer(value.GetType());
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            using (var stream = new StringWriter())
            using (var writer = XmlWriter.Create(stream, settings))
            {
                serializer.Serialize(writer, value, emptyNamepsaces);
                return stream.ToString();
            }
        }*/
    }


}